using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float speed = 3.0F;
    [SerializeField]
    private float rotateSpeed = 3.0F;
    [SerializeField]
    private float gravityValue = -9.81F;
    [SerializeField]
    private float jumpHeight = 1.0F;
    private CharacterController controller;
    private bool groundedPlayer;
    private Vector3 playerVelocity;
    private Animator animController;
    private float idleChange = 0f;
    private bool crouchMode = false;
    private bool affectGravity = true;
    private bool jumping = false;
    private bool running = false;
    
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animController = GetComponent<Animator>();
       //playerVelocity = controller.velocity;
    }

    void Update()
    {
        groundedPlayer = controller.isGrounded;
        Debug.Log("GroundedPlayer: "+groundedPlayer);
        if (groundedPlayer)
        {
            animController.SetBool("TouchGround", true);
            animController.SetBool("Jump", false);
            animController.SetBool("Falling", false);
            if (playerVelocity.y < 0)
            {
                playerVelocity.y = 0f;
            }
        }
        else
        {
            animController.SetBool("Falling", true);
            animController.SetBool("TouchGround", false);
            jumping = false;
        }
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;     
        controller.Move((crouchMode?move/2:(running)?move*2:move) * Time.deltaTime * speed*(animController.GetBool("Dance")?0:1));
        if (move != Vector3.zero)
        {
            animController.SetFloat("Movement",(running?2:1));
            idleAnimationController(true);
            if (!animController.GetBool("Dance"))
            {
                gameObject.transform.forward = move;    
            }
        }
        else
        {
            animController.SetFloat("Movement", 0);
            idleAnimationController(false);
        }
        if (Input.GetKeyDown(KeyCode.X) && !running && !animController.GetBool("Dance"))
        {
            if (crouchMode)
            {
                crouchMode = false;
                animController.SetBool("Crouch", false);    
            }
            else
            {
                crouchMode = true;
                animController.SetBool("Crouch", true);
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            animController.SetBool("Dance", !animController.GetBool("Dance"));
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && !crouchMode)
        {
            running = !running;
        }
        if (Input.GetButtonDown("Jump") && groundedPlayer && !animController.GetBool("Jump"))
        {       
            //playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            animController.SetBool("Jump", true);
        }

        if (jumping)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }
        else
        {
            controller.height = 1.83f;
        }
        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
        //animationControl();
    }

    public void jumpStart()
    {
        jumping = true;
    }
    private void idleAnimationController(bool reset)
    {
        if (reset)
        {
            idleChange = 0f;
            animController.SetFloat("IdleParameter", idleChange);
        }
        else
        {
            idleChange += Time.deltaTime;
            animController.SetFloat("IdleParameter", idleChange);
            if (idleChange >= 5) idleChange = 5f;
        }
    }
}
