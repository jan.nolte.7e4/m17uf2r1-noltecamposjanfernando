using StarterAssets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour
{
    public GameObject lookDirection;
    private GameObject player = null;
    private Animator _animator;
    private NavMeshAgent navMeshAgent;
    private EnemyState enemyState;
    private Vector3 lasPlayerPos;
    private Vector3 startingSpot;   
    void Start()
    {
        _animator = gameObject.GetComponent<Animator>();
        startingSpot = gameObject.transform.position;
        navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        _animator.SetFloat("MotionSpeed", 4);
        if (player == null)
        {        
            enemyState = EnemyState.searching;
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if(enemyState == EnemyState.patrollToPosition)
            {
                enemyState = EnemyState.searching;
            }
            else
            {
                enemyState = EnemyState.patrollToPosition;
            }
            
        }
        enemyActions();
    }
    private void searchPlayer()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (player)
        {
            enemyState = EnemyState.following;
        }
        
    }
    private void enemyActions()
    {       
        switch (enemyState)
        {
            case EnemyState.searching:
                searchPlayer();
                break;
            case EnemyState.attacking:
                _animator.SetFloat("Speed", navMeshAgent.velocity.magnitude);
                _animator.SetBool("Attack", true);
                gameObject.transform.LookAt(player.transform);
                break;
            case EnemyState.following:
                navMeshAgent.destination = player.transform.position;                
                if (EnemyArriveToPos())
                {
                    enemyState = EnemyState.attacking;
                    lasPlayerPos = player.transform.position;
                }
                else
                {
                    _animator.SetFloat("Speed", navMeshAgent.velocity.magnitude);
                }
                break;
            case EnemyState.patrollToPosition:
                navMeshAgent.destination = startingSpot;
                if (EnemyArriveToPos())
                {
                    gameObject.transform.LookAt(lookDirection.transform);
                    _animator.SetFloat("Speed", navMeshAgent.velocity.magnitude);
                }
                break;
        }     
    }  
    public bool EnemyArriveToPos()
    {
        return navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance && !navMeshAgent.pathPending;
    }
    private enum EnemyState
    {
        following,
        searching,
        attacking,
        patrollToPosition
    }
    private void ResetAttack()
    {
        if (lasPlayerPos != player.transform.position)
        {
            _animator.SetBool("Attack", false);
            enemyState = EnemyState.searching;
        }
    }
}
